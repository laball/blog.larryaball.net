<!DOCTYPE html>
<html ng-app="modalApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <title>The Blog</title>
        {{ HTML::style('css/style.css') }}
        {{ HTML::style('css/sticky-footer.css') }}
       <!--[if lt IE9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            <div class="container">
            <h1>The Blog</h1>
            <p>By Larry Ball</p>
            </div>
        </header>
        <main class="container">
            @yield('content')
        </main>
        <footer>
            <div class="container">
                 &copy; {{ date('Y') }} Some Company | {{ link_to_route('admin.posts.index', 'Admin') }}
            </div>
        </footer>
    </body>
</html>