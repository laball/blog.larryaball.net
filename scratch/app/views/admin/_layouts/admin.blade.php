<!DOCTYPE html>
<html ng-app="modalApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <title>The Blog Admin Panel</title>
        {{ HTML::style('css/normalize.css') }}
        {{ HTML::style('css/bootstrap.min.css') }}
        {{ HTML::style('css/bootstrap-theme.min.css') }}
        {{ HTML::style('css/admin.css') }}
        {{ HTML::style('css/cs-select.css') }}
        {{ HTML::style('css/cs-skin-border.css') }}
        {{ HTML::style('css/modal-fix.css') }}
        {{ HTML::style('css/sticky-footer.css') }}
       <!--[if lt IE9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            <div class="container">
            {{ link_to_route('admin.logout', 'Logout', array(), array('class' => 'logout')) }}
            <h1>The Blog Admin Panel</h1>
            </div>
        </header>
        <main class="container">
            @yield('content')
        </main>
        <footer>
            <div class="container">
                 &copy; {{ date('Y') }} Some Company {{ link_to_route('home', 'View Blog', array(), array('class' => 'home-link')) }}
            </div>
        </footer>
        {{ HTML::script('js/vendor/angular/angular.js') }}
        {{ HTML::script('js/vendor/angular-bootstrap/ui-bootstrap-custom-0.10.0.js') }}
        {{ HTML::script('js/vendor/angular-bootstrap/ui-bootstrap-custom-tpls-0.10.0.js') }}
        {{ HTML::script('js/controllers/app.js') }}
        {{ HTML::script('js/controllers/main.js') }}
        {{ HTML::script('js/controllers/about.js') }}
        {{ HTML::script('js/classie.js') }}
        {{ HTML::script('js/selectFx.js') }}
        <script>
            (function() {
                [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
                    new SelectFx(el);
                } );
            })();
        </script>
        <script>
            (function() {
                // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
                if (!String.prototype.trim) {
                    (function() {
                        // Make sure we trim BOM and NBSP
                        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                        String.prototype.trim = function() {
                            return this.replace(rtrim, '');
                        };
                    })();
                }

                [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
                    // in case the input is already filled..
                    if( inputEl.value.trim() !== '' ) {
                        classie.add( inputEl.parentNode, 'input--filled' );
                    }

                    // events:
                    inputEl.addEventListener( 'focus', onInputFocus );
                    inputEl.addEventListener( 'blur', onInputBlur );
                } );

                function onInputFocus( ev ) {
                    classie.add( ev.target.parentNode, 'input--filled' );
                }

                function onInputBlur( ev ) {
                    if( ev.target.value.trim() === '' ) {
                        classie.remove( ev.target.parentNode, 'input--filled' );
                    }
                }
            })();
        </script>
    </body>
</html>