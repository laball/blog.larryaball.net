@extends('admin._layouts.admin')

@section('content')
<h1>Posts</h1>
{{ link_to_route('admin.posts.create', 'Create New Post', array(), array('class' => 'create-post')) }}

@if(count($posts))
    <ul ng-controller="ModalCtrl">
   <script type="text/ng-template" id="modalContent.html">
        <div class="modal-header">
            <h3>Confirm Deletion</h3>
        </div>
        <div class="modal-body">
          Are You Sure?
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-click="ok()">OK</button>
            <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        </div>
    </script>
    @foreach($posts as $post)
        <li>

        {{ Form::open(array('route' => array('admin.posts.destroy', $post->id), 'method' => 'delete',
            'class' => 'destroy')) }}
        {{ Form::submit('Delete', array('id'=> 'id_'.$post->id, 'ng-click-confirm' => 'clickConfirm($event)')) }}
        {{ Form::close() }}

        {{ link_to_route('admin.posts.edit', $post->title, array($post->id)) }}

        </li>
    @endforeach
    </ul>
@endif

@stop