<ul>
    <li>
        {{ Form::label('user_id', 'Author') }}
        {{ Form::select('user_id', User::lists('name', 'id'), null, array('class' => 'cs-select cs-skin-border')) }}
        {{ $errors->first('user_id', '<p class="error">:message</p>') }}
    </li>
    <li>
        {{ Form::label('title', 'Title', array('class' => 'input__label input__label--ichiro')) }}
        {{ Form::text('title', null, array('class' => 'input__field input__field--ichiro')) }}
        {{ $errors->first('title', '<p class="error">:message</p>') }}
    </li>
    <li>
        {{ Form::label('body', 'Body', array('class' => 'input__label input__label--ichiro')) }}
        {{ Form::textarea('body', null, array('class' => 'input__field input__field--ichiro')) }}
        {{ $errors->first('body', '<p class="error">:message</p>') }}
    </li>
    <li>
        {{ Form::submit('Save') }}
    </li>
</ul>