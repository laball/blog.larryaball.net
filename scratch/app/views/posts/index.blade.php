@extends('_layouts.default')

@section('content')

@foreach($posts as $post)
<article>
    <h2>{{ link_to_route('post', $post->title, array($post->id)) }}</h2>
    <p class="created_at">Created on {{{ date('m-d-Y', strtotime($post->created_at)) }}} by {{{ $post->user->name }}}</p>
    <p>{{{ Str::words($post->body, '50', '...') }}}</p>
    <p>{{ link_to_route('post', 'Read More &rsaquo;', array($post->id), array('class' => 'read-more')) }}</p>
</article>
@endforeach
@stop