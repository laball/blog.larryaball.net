/**
 * Created by lball on 10/25/14.
 */
angular.module('modalApp',[
    'ui.bootstrap',
    'clickConfirmModule'
]);
angular.module('clickConfirmModule', [
    'ui.bootstrap'
])
    .controller('ModalCtrl', function($scope, $modal) {
        $scope.clickConfirm = {};
    })
    .directive('ngClickConfirm', ['$modal', '$http', '$location',
      function($modal, $http, $location){
          var ModalInstanceCtrl = function($scope, $modalInstance){
              $scope.ok = function () {
                  $modalInstance.close();
              };

              $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
              };
          };
          return{
              priority: -1,
              restrict: 'A',  //'A' - only matches attribute name
                              //'E' - only matches element name
                              //'C' - only matches class name
              scope:{
                  ngClickConfirm:"&"
              },
              link: function(scope, element, attrs){
                  element.bind('click', function(event){
                      event.preventDefault();
                      $modal.open({
                          templateUrl: 'modalContent.html',
                          controller: ModalInstanceCtrl,
                          size:'sm',
                          backdrop: true,
                          backdropClick: true,
                          dialogFade: true,
                          keyboard: true,
                          resolve: {} // empty storage
                      })
                      .result.then(function(){
                          var id = event.target.id.split('_');
                          var deletePath = '/admin/posts/' + id[1];
                          $http.delete(deletePath).success(function(response){
                              var elem = document.getElementById(event.target.id);
                              angular.element(elem).parent().parent().remove();
                              $location.path('/');
                          }).error(function(err){
                              console.log(err);
                              $location.path('/');
                          });
                      }, function(){
                        
                      });
                  });
              }
          }
    }]);
