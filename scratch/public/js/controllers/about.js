'use strict';

/**
 * @ngdoc function
 * @name modalApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the modalApp
 */
angular.module('modalApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
