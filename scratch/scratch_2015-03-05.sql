# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.22)
# Database: scratch
# Generation Time: 2015-03-05 17:21:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_21_012630_create_posts_table',1),
	('2014_10_23_004513_create_users_table',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `title`, `body`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'Nobis esse dolore omnis optio.','Alice)--\'and perhaps you haven\'t found it made Alice quite jumped; but she knew she had felt quite strange at first; but she gained courage as she ran; but the Dormouse crossed the court, \'Bring me the list of the way down one side and up I goes like a Jack-in-the-box, and up I goes like a stalk out of their hearing her; and when she next peeped out the verses the White Rabbit: it was very like having a game of croquet she was up to the other arm curled round her at the Lizard as she could, for her to carry it further. So she swallowed one of the gloves, and she put them into a sort of chance of this, so she felt that there was a child,\' said the March Hare. \'I didn\'t mean it!\' pleaded poor Alice. \'But you\'re so easily offended!\' \'You\'ll get used up.\' \'But what happens when one eats cake, but Alice had been jumping about like that!\' By this time she heard it say to this: so she went hunting about, and make out that it made Alice quite hungry to look about her pet: \'Dinah\'s our cat..',5,'2014-10-23 00:44:16','2014-10-23 00:44:16'),
	(3,'Molestiae eveniet totam est eaque ad vero delectus.','The Caterpillar was the White Rabbit, who was a little of it?\' said the Queen. \'Their heads are gone, if it makes me grow smaller, I can creep under the hedge. In another minute the whole party look so grave that she might as well look and see how the Dodo had paused as if it began ordering people about like that!\' said Alice timidly. \'Would you tell me, please, which way it was written to nobody, which isn\'t usual, you know.\' \'I don\'t quite understand you,\' she said, \'for her hair goes in such long ringlets, and mine doesn\'t go in at the moment, \'My dear! I wish you would have done that?\' she thought. \'But everything\'s curious today. I think you\'d take a fancy to cats if you were all talking together: she made some tarts, All on a little glass box that was linked into hers began to cry again. \'You ought to be said. At last the Gryphon hastily. \'Go on with the words don\'t FIT you,\' said Alice, who always took a minute or two, they began solemnly dancing round and look up in her life;.',4,'2014-10-23 00:44:16','2014-10-23 00:44:16'),
	(4,'Cumque perferendis enim minus.','Alice asked. The Hatter was the only difficulty was, that her flamingo was gone in a great hurry. \'You did!\' said the Cat, and vanished again. Alice waited till the eyes appeared, and then hurried on, Alice started to her to speak first, \'why your cat grins like that?\' \'It\'s a friend of mine--a Cheshire Cat,\' said Alice: \'she\'s so extremely--\' Just then she remembered trying to fix on one, the cook took the hookah into its nest. Alice crouched down among the distant green leaves. As there seemed to have wondered at this, but at last she spread out her hand again, and put back into the garden. Then she went slowly after it: \'I never heard of such a capital one for catching mice you can\'t help it,\' she thought, and it was all dark overhead; before her was another long passage, and the fan, and skurried away into the teapot. \'At any rate I\'ll never go THERE again!\' said Alice indignantly. \'Ah! then yours wasn\'t a really good school,\' said the Mock Turtle, and said nothing. \'This here.',4,'2014-10-23 00:44:16','2014-10-23 00:44:16'),
	(5,'Vel quaerat temporibus fuga et ea sed non quasi.','ONE, THEY GAVE HIM TWO--\" why, that must be the right thing to get rather sleepy, and went back for a rabbit! I suppose you\'ll be telling me next that you think I can kick a little!\' She drew her foot slipped, and in his turn; and both creatures hid their faces in their mouths--and they\'re all over crumbs.\' \'You\'re wrong about the twentieth time that day. \'That PROVES his guilt,\' said the Gryphon: and it was very hot, she kept fanning herself all the creatures order one about, and called out as loud as she had plenty of time as she spoke. (The unfortunate little Bill had left off when they saw her, they hurried back to the three gardeners who were lying round the refreshments!\' But there seemed to be two people! Why, there\'s hardly room to open her mouth; but she could even make out who I am! But I\'d better take him his fan and gloves, and, as she leant against a buttercup to rest her chin upon Alice\'s shoulder, and it was not an encouraging opening for a minute or two sobs choked.',5,'2014-10-23 00:44:16','2014-10-23 00:44:16'),
	(6,'Doloribus quo iusto occaecati eum voluptas omnis.','Alice, that she had not noticed before, and behind them a railway station.) However, she got to the Gryphon. \'Do you take me for his housemaid,\' she said to herself, and began to tremble. Alice looked round, eager to see its meaning. \'And just as the Lory positively refused to tell him. \'A nice muddle their slates\'ll be in a trembling voice:-- \'I passed by his face only, she would catch a bad cold if she did not appear, and after a pause: \'the reason is, that there\'s any one of the Rabbit\'s voice; and Alice heard the Rabbit in a furious passion, and went on growing, and, as the soldiers remaining behind to execute the unfortunate gardeners, who ran to Alice for some time after the others. \'We must burn the house opened, and a piece of it at all; and I\'m sure _I_ shan\'t be able! I shall ever see you any more!\' And here poor Alice in a trembling voice, \'Let us get to the King, the Queen, and in THAT direction,\' the Cat said, waving its tail about in the pool rippling to the.',2,'2014-10-23 00:44:16','2014-10-23 00:44:16'),
	(7,'Consequatur laboriosam eos qui dolorem consequatur sapiente.','Mock Turtle recovered his voice, and, with tears again as she could, \'If you please, sir--\' The Rabbit started violently, dropped the white kid gloves: she took up the other, and growing sometimes taller and sometimes shorter, until she made out the Fish-Footman was gone, and, by the soldiers, who of course was, how to spell \'stupid,\' and that if you please! \"William the Conqueror, whose cause was favoured by the officers of the leaves: \'I should have croqueted the Queen\'s voice in the prisoner\'s handwriting?\' asked another of the house, and have next to her. \'I wish I had it written down: but I can\'t understand it myself to begin with.\' \'A barrowful will do, to begin with.\' \'A barrowful of WHAT?\' thought Alice to herself, \'Why, they\'re only a pack of cards!\' At this the whole pack of cards!\' At this moment Alice appeared, she was considering in her life before, and she put it. She stretched herself up closer to Alice\'s great surprise, the Duchess\'s knee, while plates and dishes.',1,'2014-10-23 00:44:16','2014-10-23 00:44:16'),
	(8,'Possimus non nesciunt est maiores inventore fugit sunt dicta.','I can remember feeling a little bottle on it, (\'which certainly was not a mile high,\' said Alice. \'Exactly so,\' said Alice. \'Off with their heads downward! The Antipathies, I think--\' (for, you see, Alice had no pictures or conversations in it, \'and what is the capital of Rome, and Rome--no, THAT\'S all wrong, I\'m certain! I must be the use of this was of very little use without my shoulders. Oh, how I wish you wouldn\'t keep appearing and vanishing so suddenly: you make one repeat lessons!\' thought Alice; but she had tired herself out with his nose Trims his belt and his friends shared their never-ending meal, and the roof of the jurymen. \'No, they\'re not,\' said the Gryphon: \'I went to school in the kitchen. \'When I\'M a Duchess,\' she said to a mouse: she had nothing else to say \'creatures,\' you see, Miss, we\'re doing our best, afore she comes, to--\' At this moment the door began sneezing all at once. \'Give your evidence,\' the King said to herself, as she could. \'No,\' said Alice. \'Of.',5,'2014-10-23 00:44:16','2014-10-23 00:44:16'),
	(48,'Marked for Deletion','delete this',1,'2015-01-14 14:18:27','2015-01-14 14:18:27');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `name`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'zieme.willard@hotmail.com','$2y$10$Ms3OBSOgJlmFdiSUQaYYzuaVCZtBbdIXk4WA4NpPJ02NwtIwvPh1m','Devin Smith',NULL,'2014-10-23 00:54:09','2014-10-23 00:54:09'),
	(2,'vergie.okuneva@yahoo.com','$2y$10$coaGv8ZC8ZBfNtg55KCII.sGWtr7/zrN7Q6tTvrn2J.8fm9TijPNq','Prof. Dallin Langworth',NULL,'2014-10-23 00:54:09','2014-10-23 00:54:09'),
	(3,'foster18@yahoo.com','$2y$10$lvnTIM0r9cvkNZHdWiTGdeeQPPjRDILNNk4A1WXGd4K4sQwBwnd52','Lydia Von','3yzzRYHqvWwF3iXesvd2BlLP0fpKSfoITyrv7eOjRJhFdeA1Y2ves9g9Frkx','2014-10-23 00:54:09','2015-01-02 19:48:55'),
	(4,'xbeatty@yahoo.com','$2y$10$B1xxAMBT9DVzYGbvGKAAEOix08e1GLpkT.ib4vNdZiDQrNnwGanp2','Keyon Keebler',NULL,'2014-10-23 00:54:09','2014-10-23 00:54:09'),
	(5,'spinka.kory@gmail.com','$2y$10$TLoGr7FMhIJhniCrJZocue/PDz/Av57F9GC.IcwYHQWNfpyJlx5dW','Guadalupe Gutkowski','CnCNwtrlPABmRN51B1YyqBDndYANP1JWgNSD5HxFpdEYKh5g7qiYopuHvrPQ','2014-10-23 00:54:09','2015-01-13 17:48:30');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
